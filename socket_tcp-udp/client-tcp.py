import socket
import sys

def main():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
    except socket.error as error:
        print(error)
        sys.exit()

    print('Socket Criado!')

    host = input('Digite o HOST ou IP a ser conectado: ')
    port = input('Digite a porta a ser conectado: ')

    try:
        s.connect((host, int(port)))
        print('Cliente TCP conectado com sucesso')
    except socket.error as error:
        print(error)
        sys.exit()


if __name__ == '__main__':
    main()