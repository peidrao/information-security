import socket


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

print('Socket criado com sucesso\n')

host = 'localhost'
port = 5432

s.bind((host, port))
text = 'Server: Hello Client\n'


while 1:
    data, end = s.recvfrom(4096)

    if data:
        print('Server send message\n')
        s.sendto(data + (text.encode()), end)