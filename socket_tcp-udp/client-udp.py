import socket
from typing import DefaultDict

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

host = 'localhost'
port = 5433
text = "Hello server!\n"

try:
    print(f'Client: {text}\n')
    s.sendto(text.encode(), (host, 5432))
    data, server = s.recvfrom(4096)
    data = data.decode()
    print(f'Client: {data}\n')
finally:
    print('Client: fechando a conexão')
    s.close()