from threading import Thread

def car(velocity, pilot):
    path = 0
    while path <= 100:
        print(f'Car: {path} | Pilot: {pilot}')
        path += velocity


t_car1 = Thread(target=car, args=[20, 'Flask'])
t_car2 = Thread(target=car, args=[5, 'Django'])

t_car2.start()
t_car1.start()

# car1(50)
# car2(25)