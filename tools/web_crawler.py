import operator, requests
from collections import Counter
from bs4 import BeautifulSoup

def start(url):
    worldlist = []
    source_code = requests.get(url).text

    soup = BeautifulSoup(source_code, 'html.parser')

    for each_text in soup.findAll('div', {'class': 'entry-content'}):
        content = each_text.text
        words = content.lower().split()

        for each_word in words:
            worldlist.append(each_word)
        clean_wordlist(worldlist)


def clean_wordlist(wordlist):
    clean_list = []
    for word in wordlist:
        symbols = '!@#$%&*({[]}+=_-)^~?/\*'
        for i in range(0, len(symbols)):
            word = word.replace(symbols[i], '')

        if len(word) > 0:
            clean_list.append(word)
    create_dict(clean_list)


def create_dict(clean_list):
    word_count = {}
    for word in clean_list:
        if word in word_count:
            word_count[word] += 1
        else:
            word_count[word] = 1

    for k, v in sorted(word_count.items(), key= operator.itemgetter(1)):
        print(f'{k} : {v}')

    c = Counter(word_count)

    top = c.most_common(10)
    print(top)



if __name__ == '__main__':
    start('https://www.geeksforgeeks.org/python-programming-language/?ref=leftbar')


