import ipaddress

ip  = '192.168.0.1/24'

network = ipaddress.ip_network(ip, strict=False)
print(network)