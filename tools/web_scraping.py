import requests
from bs4 import BeautifulSoup


site  = requests.get('https://www.climatempo.com.br/').content

soup = BeautifulSoup(site, 'html.parser')

temp = soup.find('a', class_='_flex _align-center _margin-b-10"')

print(temp.string)