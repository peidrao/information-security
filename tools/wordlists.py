import itertools


string = input('String: ')

result = itertools.permutations(string, len(string))

for i in result:
    print(''.join(i))


